import {calculateBonuses} from "./bonus-system";
const assert = require('assert');

const S = "Standard";
const P = "Premium";
const D = "Diamond";

describe('Bonus system tests', () => {
    let app;

    test('Standard sub 10000',(done) => {
        let a = Math.random()*9999
        expect(calculateBonuses(S,a)).toEqual(0.05);
        done();
    })

    test('Standard eq 10000',(done) => {
        let a = 10000
        expect(calculateBonuses(S,a)).toEqual(0.05*1.5);
        done();
    })

    test('Standard sub 50000',(done) => {
        let a = 10000 + 39999*Math.random()
        expect(calculateBonuses(S,a)).toEqual(0.05*1.5);
        done();
    })

    test('Standard eq 50000',(done) => {
        let a = 50000
        expect(calculateBonuses(S,a)).toEqual(0.05*2);
        done();
    })

    test('Standard sub 100000',(done) => {
        let a = 50000 + 49999*Math.random()
        expect(calculateBonuses(S,a)).toEqual(0.05*2);
        done();
    })

    test('Standard eq 100000',(done) => {
        let a = 100000
        expect(calculateBonuses(S,a)).toEqual(0.05*2.5);
        done();
    })

    test('Standard over 100000',(done) => {
        let a = 100000 + 999999*Math.random()
        expect(calculateBonuses(S,a)).toEqual(0.05*2.5);
        done();
    })

    test('Premium over 100000',(done) => {
        let a = 100000 + 999999*Math.random()
        expect(calculateBonuses(P,a)).toEqual(0.1*2.5);
        done();
    })

    test('Diamond over 100000',(done) => {
        let a = 100000 + 999999*Math.random()
        expect(calculateBonuses(D,a)).toEqual(0.2*2.5);
        done();
    })

    test('Unknown',(done) => {
        let str = "s"+Math.random()*100000
        let a = 999999*Math.random()
        expect(calculateBonuses(str,a)).toEqual(0);
        done();
    })
});

